package maier.andreea.g30125.l6.e5;


public class Brick {

    private int height;
    private int width;

    public Brick(int hight, int width) {
        this.height = hight;
        this.width = width;
    }


    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }


}

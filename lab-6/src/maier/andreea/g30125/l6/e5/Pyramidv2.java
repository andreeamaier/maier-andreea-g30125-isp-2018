package maier.andreea.g30125.l6.e5;

import javax.swing.*;
import java.awt.*;

public class Pyramidv2 extends JFrame {

    Brick brick;
    int noOfBricks;


    public Pyramidv2(Brick brick, int noOfBricks) {
        this.brick = brick;
        this.noOfBricks = noOfBricks;
    }

    public void paint(Graphics g){
        super.paint(g);

        int currentX = noOfBricks*brick.getWidth()/2;//- brick.getWidth()/2;
        int currentY = 80;
        int startX = currentX;

        int level = 1;
        while (noOfBricks > 0) {
            if ((level+1 <= noOfBricks-level) || (level == noOfBricks)) {
                //startX = currentX;
                for (int i=0; i<level; i++){
                    g.drawRect(currentX,currentY, brick.getWidth(), brick.getHeight());
                    currentX+= brick.getWidth();
                }
                noOfBricks-=level;
                level++;
                currentY+=brick.getHeight();
                currentX=startX-brick.getWidth()*(level-1)/2;
                //startX = currentX;
            }
            else {
                currentX=startX-brick.getWidth()*(noOfBricks-1)/2;
                for(int i=0; i<noOfBricks; i++){
                    g.drawRect(currentX,currentY, brick.getWidth(), brick.getHeight());
                    currentX+= brick.getWidth();
                }
                noOfBricks=0;
            }
        }
    }


}

package maier.andreea.g30125.l6.e5;

import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Enter the number of bricks, their width and their height:");
        Scanner in = new Scanner(System.in);
        System.out.println("Number of bricks:");
        int noOfBricks = in.nextInt();
        System.out.println("Width:");
        int width = in.nextInt();
        System.out.println("Height:");
        int height = in.nextInt();
        in.close();

        Brick brick = new Brick(height,width);

        //Class Pyramid prints the pyramid on the screen using "x" as bricks
        Pyramid pyramid = new Pyramid(brick,noOfBricks);
        pyramid.buildPyramid();

        //Class Pyramidv2 prints the pyramid on screen using JFrame
        Pyramidv2 pyramid2 = new Pyramidv2(brick,noOfBricks);

        pyramid2.setTitle("Pyramid");
        pyramid2.setSize(20000,1000);
        pyramid2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pyramid2.setVisible(true);


    }
}

package maier.andreea.g30125.l6.e5;

public class Pyramid {

    Brick brick;
    int noOfBricks;
    //DrawingBoard board;


    public Pyramid(Brick brick, int noOfBricks) {
        this.brick = brick;
        this.noOfBricks = noOfBricks;
    }


    public void buildPyramid(){
        int level = 1;
        int space = noOfBricks;
        while (noOfBricks > 0){
            //brick.setX(300-brick.getWidth()/2);
            //brick.setY(40);
            if ((level+1 <= noOfBricks-level) || (level == noOfBricks)){
                for (int i = 0; i<space; i++)
                    System.out.print(" ");
                for(int i=0; i<level; i++){
                    System.out.print("x ");
                }
                space--;
                noOfBricks-=level;
                level++;
                System.out.println();
            }
            else {
                //space++;
                space=space-(noOfBricks-level);
                for (int i = 0; i<space; i++)
                    System.out.print(" ");
                for (int i = 0; i < noOfBricks; i++) {
                    System.out.print("x ");
                }
                noOfBricks = 0;
            }

        }
    }
}

package maier.andreea.g30125.l6.e1;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,90,60, "C386", true, 90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,200,180,"C875", true,100);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLACK,10,35,"R845", false,50);
        b1.addShape(s3);

        b1.deleteByID("C875");
    }
}
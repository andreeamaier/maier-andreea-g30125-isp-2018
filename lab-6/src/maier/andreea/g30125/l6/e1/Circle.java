package maier.andreea.g30125.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int x, int y, String id, Boolean fill, int radius) {
        super(color,x,y,id,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        if (getFill() == true) {
        System.out.println("Drawing a filled circle "+this.radius+" "+getColor().toString());}
        else
            System.out.println("Drawing a simple circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
    }
}

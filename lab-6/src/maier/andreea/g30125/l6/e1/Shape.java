package maier.andreea.g30125.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    private String id;
    private Boolean fill; //true if the shape should be drawn filled, false otherwise

    public Shape(Color color, int x, int y, String id, Boolean fill) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getFill() {
        return fill;
    }

    public void setFill(Boolean fill) {
        this.fill = fill;
    }

    public abstract void draw(Graphics g);
}

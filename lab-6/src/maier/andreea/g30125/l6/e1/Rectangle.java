package maier.andreea.g30125.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int x, int y, String id, Boolean fill, int length) {
        super(color,x,y,id,fill);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        if (getFill() == true) {
            System.out.println("Drawing a filled rectangle " + length + " " + getColor().toString());
        } else
            System.out.println("Drawing a simple rectangle " + length + " " + getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
    }
}

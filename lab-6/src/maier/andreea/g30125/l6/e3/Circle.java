package maier.andreea.g30125.l6.e3;

import java.awt.*;

public class Circle implements Shape{
    private Color color;
    private int x;
    private int y;
    private String id;
    private Boolean fill; //true if the shape should be drawn filled, false otherwise
    private int radius;
    Graphics g;

    public Circle(Color color, int x, int y, String id, Boolean fill, int radius) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
        this.radius = radius;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw(Graphics g) {
        if (fill == true) {
            System.out.println("Drawing a filled circle "+this.radius+" "+color.toString());}
        else
            System.out.println("Drawing a simple circle "+this.radius+" "+color.toString());
        g.setColor(getColor());
        g.drawOval(x,y,radius,radius);
    }

}

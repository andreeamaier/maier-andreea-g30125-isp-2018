package maier.andreea.g30125.l6.e6;

import javax.swing.*;
import java.awt.*;

public class Fractal extends JFrame {

    private int x;
    private int y;
    private int length;

    public Fractal(int x, int y, int length) {
        this.x = x;
        this.y = y;
        this.length = length;
    }

    //Fractal is composed of a black filled square and a pink filled inscribed circle and it's
    //drawn as long as the square's length (circle's radius) is bigger than 10
    public void paint(Graphics g){
        while(length>10){
            g.setColor(Color.BLACK);
            g.fillRect(x,y,length,length);
            g.setColor(Color.PINK);
            g.fillOval(x+1,y+1,length-2,length-2);
            //paint(g/*(int)(x-length-(Math.sqrt(2*length*length))),(int)(y-length-(Math.sqrt(2*length*length))),(int)(length-Math.sqrt(2*length*length))*/);
            x=(int)(x+(length-(Math.sqrt(length*length/2)))/2);
            y=(int)(y+(length-(Math.sqrt(length*length/2)))/2);
            length=(int)(Math.sqrt(length*length/2));
            //System.out.println(length);
        }
    }


    public static void main(String[] args) {

        Fractal element = new Fractal(200,200,400);
        element.setTitle("Fractal");
        element.setSize(1000,1000);
        element.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        element.setVisible(true);
    }
}

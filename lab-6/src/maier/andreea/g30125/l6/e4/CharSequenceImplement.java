package maier.andreea.g30125.l6.e4;

public class CharSequenceImplement implements CharSequence{

    private char[] chars;
    private int start;
    private int stop;

    public CharSequenceImplement(char[] chars) {
        this.chars = chars;
    }

    public CharSequenceImplement(char[] chars, int start, int stop) {
        this.chars = chars;
        this.start = start;
        this.stop = stop;
    }

    @Override
    public int length() {
        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index];
    }

    @Override
    public CharSequence subSequence(int startSub, int stopSub) {
        start=0;
        stop=this.length();
        CharSequenceImplement subsequence = new CharSequenceImplement(chars,start+startSub,stop-stopSub);
        return subsequence;
    }

    @Override
    public String toString() {
        return new String(this.chars, this.start, this.stop);
    }
}

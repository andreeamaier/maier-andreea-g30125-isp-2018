package maier.andreea.g30125.l10.e6;
//sinconizare wait notify(apelate din blocuri sinc), threads, jframe
//thread,ui cu 2 butoane si un texfield
//in thread sin secunda in secunda face setText la textfield
//cand se apasa pe buton, threadul trebuie sa faca pauza
//(se apeleaza o metoda in care este o var booleana pauza=true;
//if p=true wait
//if p=false notify

public class Chronometer implements Runnable{
    int k=0;
    Integer syncObj = 0;
    boolean p = true;

    Chronometer(){
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true){
            if(p){
                synchronized (syncObj){
                    try {
                        syncObj.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            } else {
                synchronized (this){
                    this.notify();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
            k++;
        }
    }

    public void changeState() {
        p = !p;
        if(p){
            synchronized (syncObj){
                syncObj.notify();
            }
        }
    }
}

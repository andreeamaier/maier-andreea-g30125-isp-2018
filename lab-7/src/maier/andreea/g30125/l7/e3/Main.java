package maier.andreea.g30125.l7.e3;


import maier.andreea.g30125.l7.e1.BankAccount;

import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Bank b = new Bank();

        b.addAccount("Ana", 250.5);
        b.addAccount("Dan",1000);
        b.addAccount("Ioana", 250.4);
        b.addAccount("Ion",750);

        //I am using a TreeSet, so if I add an account with
        //the same balance as one that I have already added, it
        //sholdn't be added in the Bank because TreeSet allows
        //only one entry based on the compareTo method.

        b.addAccount("Marius",750); //this won't be printed on screen

        System.out.println("These are the accounts from the Bank:");
        b.printAccounts();

        System.out.println("\nAccounts with the balance between 300 and 1000");
        b.printAccounts(300,1000);

        System.out.println("\nAccount found based on the name");
        BankAccount ba = b.getAccount("Dan");
        System.out.println("Balance is: " + ba.getBalance());

        TreeSet <BankAccount> accountsList = new TreeSet<>(new ownerComp());
        TreeSet <BankAccount> accountsListBeforeSorting = b.getAllAccounts();
        accountsList.addAll(accountsListBeforeSorting);
        System.out.println("\nNew order:");
        for (BankAccount bankAcc : accountsList){
            System.out.println(bankAcc.getOwner() + " " + bankAcc.getBalance());
        }
    }

}

class ownerComp implements Comparator<BankAccount>{

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.getOwner().compareTo(o2.getOwner());
    }
}

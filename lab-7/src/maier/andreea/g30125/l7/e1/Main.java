package maier.andreea.g30125.l7.e1;

public class Main {

    public static void main(String[] args) {

        BankAccount b1 = new BankAccount("Ana");
        BankAccount b2 = new BankAccount("Dan");

        b1.deposit(500);
        b1.deposit(289.3);
        b2.deposit(985);
        b2.withdraw(540);

        System.out.println(b1.getBalance());
        System.out.println(b2.getBalance());

        b2.withdraw(1000);

        BankAccount b3 = b1;

        if (b1.equals(b2)){
            System.out.println("Equal");}
            else
            System.out.println("Not equal");


        if (b1.equals(b3)){
            System.out.println("Equal");}
        else
            System.out.println("Not equal");

        System.out.println("b1 " + b1.hashCode());
        System.out.println("b2 " + b2.hashCode());
    }
}

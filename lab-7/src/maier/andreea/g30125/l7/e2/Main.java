package maier.andreea.g30125.l7.e2;

import maier.andreea.g30125.l7.e1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        Bank b = new Bank();
        b.addAccount("Ana", 250.5);
        b.addAccount("Dan",1000);
        b.addAccount("Ioana", 250.4);
        b.addAccount("Ion",750);
        b.addAccount("Dan",1000);

        b.printAccounts();
        System.out.println();
        b.printAccounts(250.4,1200);
        BankAccount ba1 = b.getAccount("Dan");
        System.out.println(ba1.getBalance());

        ArrayList<BankAccount> bankAccounts = b.getAllAccounts();
        Collections.sort(bankAccounts,ownerComp);

        System.out.println("\n Name sorted list:");
        for (BankAccount ba : bankAccounts){
            System.out.println(ba.getOwner() + " " + ba.getBalance());
        }

    }

    public static Comparator<BankAccount> ownerComp = new Comparator<BankAccount>() {
        @Override
        public int compare(BankAccount o1, BankAccount o2) {
            String ownerName1 = o1.getOwner();
            String ownerName2 = o2.getOwner();

            return ownerName1.compareTo(ownerName2);
        }
    };
}

package maier.andreea.g30125.l7.e4;

public class Definition {

    private String description;
    //private Word word;

    public Definition(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

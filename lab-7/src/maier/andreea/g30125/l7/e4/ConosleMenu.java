package maier.andreea.g30125.l7.e4;

import java.util.Scanner;

public class ConosleMenu {

    public static void main(String[] args) {

        Dictionary d = new Dictionary();
        int option = 1; //Option is set on 1, so the menu can be displayed at least once.
        String word;
        String definition;

        while (option!=0){
            displayMenu();
            Scanner in = new Scanner(System.in);
            option = in.nextInt();

            switch (option) {
                case 1: {           //add word
                    System.out.println("Enter the word: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    System.out.println("Enter the definition: ");
                    Scanner inDef = new Scanner(System.in);
                    definition = inDef.next();
                    d.addWord(new Word(word),new Definition(definition));
                    break;
                }
                case 2: {           //displays the definition for a given word
                    System.out.println("Enter the word: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    d.getDefinition(new Word(word));
                    break;
                }
                case 3: {          //displays all the words from the dictionary
                    d.getAllWords();
                    break;
                }
                case 4: {           //displays all the definitions from the dictionary
                    d.getAllDefinitions();
                    break;
                }
                case 0: {           //exits the menu
                    break;
                }
            }
        }
    }

    private static void displayMenu() {
        System.out.println("Menu:");
        System.out.println("1. Add word");
        System.out.println("2. Get definition");
        System.out.println("3. Get all words");
        System.out.println("4. Get all definitions");
        System.out.println("0. Exit");
        System.out.println("Option: ");
    }
}

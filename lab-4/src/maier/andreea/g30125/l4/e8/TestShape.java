package maier.andreea.g30125.l4.e8;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestShape {

    @Test
    public void shouldPrintStingForCircleClass() {
        Circle c1 = new Circle(2.5, "pink", true);
        assertEquals("A Circle with radius=2.5, which is a subclass of A Shape with color of pink and filled", c1.toString());
    }

    @Test
    public void shouldPrintStringForRectangleClass() {
        Rectangle r1 =  new Rectangle(3,6,"green", false);
        assertEquals("A Rectangle with width=3.0 and length=6.0, which is a subclass of A Shape with color of green and Not filled", r1.toString());
    }

    @Test
    public void shouldPrintSqareArea() {
        Square s1 = new Square(5,"purple",false);
        assertEquals(25, s1.getArea(),0.01);
    }
}

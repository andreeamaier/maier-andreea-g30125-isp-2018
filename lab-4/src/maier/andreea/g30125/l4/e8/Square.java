package maier.andreea.g30125.l4.e8;

public class Square extends Rectangle {

    public Square(){

    }

    public Square(double side) {
        super(side,side);
    }

    public Square (double side, String color, Boolean filled) {
        super(side,side,color,filled);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "A Square with side=" + this.getLength() + ", which is a subclass of " + super.toString();
    }


}

package maier.andreea.g30125.l4.e8;

public class Main {

    public static void main(String[] args) {

        Circle c1 = new Circle(2.5, "pink", true);
        Rectangle r1 =  new Rectangle(3,6,"green", false);
        Square s1 = new Square(5,"purple",false);

        System.out.println(c1.toString());
        System.out.println(r1.toString());
        System.out.println(s1.toString());
    }
}

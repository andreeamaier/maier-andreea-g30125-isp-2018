package maier.andreea.g30125.l4.e4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAuthor {

    @Test
    public void shouldReturnString() {
        Author a = new Author("Name" ,"email@gmail.com" , 'm');
        assertEquals("Name (m) at email@gmail.com" , a.toString());
    }
}

package maier.andreea.g30125.l4.e4;

public class Main {

    public static void main(String[] args) {

        Author a1 = new Author("First Author", "a1@gmail.com", 'm');
        Author a2 = new Author("Second Author", "a2@gmail.com", 'f');

        System.out.println(a1.getEmail());

    }
}

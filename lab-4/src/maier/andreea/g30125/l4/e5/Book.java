package maier.andreea.g30125.l4.e5;

import maier.andreea.g30125.l4.e4.Author;

public class Book {

    private String name;
    private Author author;
    private double price;
    private int qtyInStock = 0;

    public Book (String bookname, String name, String email, char gender, double price, int qty){
        this.author = new Author(name, email, gender);
        this.name = bookname;
        this.price = price;
        this.qtyInStock = qty;
    }

    public Book (String bookname, String name, String email, char gender, double price) {
        this.author = new Author(name, email, gender);
        this.name = bookname;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return name + " by " + author.toString();
    }
}

package maier.andreea.g30125.l4.e5;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook {

    @Test
    public void shouldHaveSamePrice(){
        Book b = new Book ("Title", "A1", "a1@gmail.com", 'm', 25) ;
        assertEquals(25, b.getPrice(), 0.01);
    }

    @Test
    public void shouldHaveSameEmail() {
        Book b = new Book ("Title", "A1", "a1@gmail.com", 'm', 25) ;
        assertEquals("Title by A1 (m) at a1@gmail.com", b.toString());
    }
}

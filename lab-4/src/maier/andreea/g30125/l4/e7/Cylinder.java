package maier.andreea.g30125.l4.e7;

import maier.andreea.g30125.l4.e3.Circle;

public class Cylinder extends Circle {

    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return pi*getRadius()*getRadius()*height;
    }

    @Override
    public double getArea() {
        return (2*pi*getRadius()*height + 2*pi*getRadius()*getRadius());
    }
}

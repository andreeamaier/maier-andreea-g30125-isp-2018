package maier.andreea.g30125.l4.e7;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCylinder {

    @Test
    public void shouldHaveSameArea(){
        Cylinder c1 = new Cylinder(1,2);
        assertEquals(18.85, c1.getArea(), 0.02);
    }
}

package maier.andreea.g30125.l4.e2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestMyPoint {

    @Test

    public void shouldHaveSameDistance() {
        MyPoint p1 = new MyPoint(3,8);
        assertEquals(3.16, p1.distance(2,5),0.01);
    }

    @Test

    public void ShouldReturnThePointCoordinates() {
        MyPoint p = new MyPoint(3,8);
        assertEquals("(3,8)", p.toString());
    }
}

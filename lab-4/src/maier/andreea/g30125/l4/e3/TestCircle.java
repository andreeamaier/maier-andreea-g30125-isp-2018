package maier.andreea.g30125.l4.e3;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCircle {

    @Test
    public void shouldHaveTheSameArea(){
        Circle c = new Circle (2);
        assertEquals(12.56, c.getArea(), 0.01);
    }
}

package maier.andreea.g30125.l4.e3;

public class Main {

    public static void main(String[] args) {

        Circle c1 = new Circle();
        Circle c2 = new Circle(2.0);

        System.out.println("Circle c1 has the radius = " + c1.getRadius());
        System.out.println("Circle c2 has the area = " + c2.getArea());

    }
}

package maier.andreea.g30125.l4.e3;

public class Circle {

    private double radius = 1.0;
    private String color = "red";
    public final static double pi = 3.14;

    public Circle() {
    }

    public Circle(double radius){
        this.radius = radius;
    }

    public Circle(String color){
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return pi*radius*radius;
    }

}

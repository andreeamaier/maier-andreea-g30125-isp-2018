package maier.andreea.g30125.l4.e9;

import becker.robots.*;

public class Diver extends Robot{


    public Diver(City city, int i, int i1, Direction direction) {
        super(city, i, i1, direction);
    }

    public void Dive() {
        this.move();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.move();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.move();
        for (int i = 0; i<3; i++) {
            this.turnLeft();
            this.turnLeft();
            this.turnLeft();
            this.turnLeft();
        }
        this.move();
        this.move();
        this.move();
        this.turnLeft();
        this.turnLeft();

    }

    public static void main(String[] args) {

        City cluj = new City();
        Wall blockAve0 = new Wall(cluj, 2, 3, Direction.EAST);
        Wall blockAve1 = new Wall(cluj, 3, 3, Direction.EAST);
        Wall blockAve2 = new Wall(cluj, 4, 3, Direction.EAST);
        Wall blockAve3 = new Wall(cluj, 4, 4, Direction.WEST);
        Wall blockAve4 = new Wall(cluj, 4, 5, Direction.SOUTH);
        Wall blockAve5 = new Wall(cluj, 4, 4, Direction.SOUTH);
        Wall blockAve6 = new Wall(cluj, 4, 5, Direction.EAST);
        Wall blockAve7 = new Wall(cluj, 2, 4, Direction.NORTH);

        Diver karel = new Diver(cluj, 1, 4, Direction.NORTH);

        karel.Dive();
    }
}
package maier.andreea.g30125.l4.e6;

public class Main {

    public static void main(String[] args) {

         String[] name = {"A1","A2","A3"};
         String[] email = {"e1","e2","e3"};
         char[] gender = {'f','f','m'};

        Book b1 = new Book("Book Title", name , email, gender, 26.5, 30);
        b1.printAuthors();
        System.out.println(b1.toString());
        for (int i=0; i<=b1.getAuthors().length; i++) {
            System.out.println(b1.getAuthors()[i].getName());
        }
    }
}

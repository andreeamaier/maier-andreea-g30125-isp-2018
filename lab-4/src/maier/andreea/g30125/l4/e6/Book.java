package maier.andreea.g30125.l4.e6;

import maier.andreea.g30125.l4.e4.Author;

import java.util.Arrays;

public class Book {

    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    public Book (String bookname, String[] name, String[] email, char[] gender, double price, int qty){
        this.authors = new Author[name.length];
        for (int i=0; i<name.length; i++) {
            this.authors[i] = new Author(name[i],email[i],gender[i]);
        }
        this.name = bookname;
        this.price = price;
        this.qtyInStock = qty;
    }

    public Book (String bookname, String[] name, String[] email, char[] gender, double price){
        this.authors = new Author[name.length];
        for (int i=0; i<name.length; i++) {
            this.authors[i] = new Author(name[i],email[i],gender[i]);
        }
        this.name = bookname;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return name + " by " + authors.length +" authors.";
    }

    public void printAuthors() {
        for (int i=0; i<authors.length; i++) {
            System.out.println(authors[i].getName());
        }
    }
}

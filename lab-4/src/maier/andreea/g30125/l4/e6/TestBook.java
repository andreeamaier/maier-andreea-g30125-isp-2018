package maier.andreea.g30125.l4.e6;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook {

    @Test
    public void shouldPrintAuthors() {
        String[] name = {"A1","A2","A3"};
        String[] email = {"e1","e2","e3"};
        char[] gender = {'f','f','m'};
        Book b1 = new Book("Book Title", name , email, gender, 26.5, 30);
        assertEquals("A1", b1.getAuthors()[0].getName());
    }


    @Test
    public void shouldPrintTitleAndAuthorsNum() {
        String[] name = {"A1","A2","A3"};
        String[] email = {"e1","e2","e3"};
        char[] gender = {'f','f','m'};
        Book b1 = new Book("Book Title", name , email, gender, 26.5, 30);
        assertEquals("Book Title by 3 authors.", b1.toString());
    }
}

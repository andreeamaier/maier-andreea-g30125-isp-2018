/*Exercise PrintNumberInWord (nested-if, switch-case): Write a program called PrintNumberInWord
which prints “ONE”, “TWO”,… , “NINE”, “OTHER” if the int variable “number” is 1, 2,… , 9, or other,
respectively. Use (a) a “nested-if” statement; (b) a “switch-case” statement.*/

package g30125.maier.andreea.l2.e2;

import java.util.Scanner;

public class Exercitiul2 {
        static void exnestedif (int number)
        {
            String no;
            if (number==1) {
                no="One";
            } else if (number==2) {
                no="Two";
            } else if (number==3) {
                no="Three";
            } else if (number==4) {
                no="Four";
            } else if (number==5) {
                no="Five";
            } else if (number==6) {
                no="Six";
            } else if (number==7) {
                no="Seven";
            } else if (number==8) {
                no="Eight";
            } else if (number==9) {
                no="Nine";
            } else  {
                no="Other";
            }
            System.out.println(no);
        }

        static void exswitch (int number)
        {
            String no;
            switch (number) {
                case 1:  no = "One";
                    break;
                case 2:  no = "Two";
                    break;
                case 3:  no = "Three";
                    break;
                case 4:  no = "Four";
                    break;
                case 5:  no = "Five";
                    break;
                case 6:  no = "Six";
                    break;
                case 7:  no = "Seven";
                    break;
                case 8:  no = "Eight";
                    break;
                case 9:  no = "Nine";
                    break;
                default: no = "Other";
                    break;
            }
            System.out.println(no);
        }


        public static void main (String[] args){
            System.out.println("Enter the number:");
            Scanner in=new Scanner(System.in);
            int n=in.nextInt();
            in.close();
            exnestedif(n);
            exswitch(n);
        }
}

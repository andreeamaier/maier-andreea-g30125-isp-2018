/*Write a program which display prime numbers between A and B, where A and B are
read from console. Display also how many prime numbers have been found.*/

package g30125.maier.andreea.l2.e3;

import java.util.Scanner;

public class Exercitiul3 {

    static boolean isPrime(int n) {
        for(int i=2;i<n;i++) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("A= ");
        Scanner in = new Scanner (System.in);
        int A = in.nextInt();
        System.out.println("B= ");
        int B = in.nextInt();
        in.close();

        for (int i=A; i<=B; i++){
            if (isPrime(i)){
                System.out.print(i+" ");
            }
        }
    }
}

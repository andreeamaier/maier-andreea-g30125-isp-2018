/*Write a program which generate a vector of 10 int elements,
sort them using bubble sort method and then display the result.*/

package g30125.maier.andreea.l2.e5;

import java.util.Random;

public class Exercitiul5 {

    static void bubbleSort(int vector[]){
        int n = vector.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (vector[j - 1] > vector[j]) {
                    temp = vector[j - 1];
                    vector[j - 1] = vector[j];
                    vector[j] = temp;
                }

            }
        }
    }

    public static void main(String[] args) {
        Random r = new Random();

        int a[] = new int[10];

        for(int i=0; i<a.length; i++){
            a[i] = r.nextInt(100);
        }

        System.out.println("Before Bubble Sort:");
        for(int i=0;i<a.length;i++){
            System.out.print(a[i] + " ");
        }

        bubbleSort(a);

        System.out.println("\nAfter Bubble Sort:");
        for(int i=0;i<a.length;i++){
            System.out.print(a[i] + " ");
        }
    }
}

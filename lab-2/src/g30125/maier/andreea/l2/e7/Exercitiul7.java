/*Write a “Guess the number” game in Java.
Program will generate a random number and will ask user to guess it.
If user guess the number program will stop. If user do not guess it program will display:
'Wrong answer, your number it too high' or 'Wrong answer, your number is too low'.
Program will allow user maximum 3 retries after which will stop with message 'You lost'.*/

package g30125.maier.andreea.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class Exercitiul7 {
    public static void main(String[] args) {

        Random r = new Random();
        int generatedNum = r.nextInt(1000);
        int triesNum = 0, num;
        System.out.println(generatedNum);

        while  (triesNum < 3) {

            triesNum++;

            System.out.println("Try no " + triesNum + ". Enter your number: ");
            Scanner in = new Scanner (System.in);
            num = in.nextInt();

            if (num < generatedNum) {
                System.out.println("Wrong answer, your number is too low.");
            } else if (num > generatedNum) {
                System.out.println("Wrong answer, your number it too high.");
            } else {
                System.out.println("You won!");
                break;
            }

            if (triesNum == 3) {
                System.out.println("You lost! The correct answer was: " + generatedNum);
            }
        }

    }
}

/*Being given an int number N, compute N! using 2 methods:

1. a non recursive method
2. a recursive method*/

package g30125.maier.andreea.l2.e6;

import java.util.Scanner;

public class Exercitiul6 {

    static int metNonRec (int num) {
        int fact = 1;
        for (int i=1; i<=num; i++) {
            fact*=i;
        }
        return fact;
    }

    static int metRec (int num) {
        if (num == 0) {
            return 1;
        } else {
            return num*metRec(num-1);
        }
    }

    public static void main(String[] args) {

        System.out.print("Enter the number N= ");
        Scanner in = new Scanner (System.in);
        int N = in.nextInt();
        in.close();

        System.out.println("\n Using a:");
        System.out.println("Non recursive method N!=" + metNonRec(N));
        System.out.println("Recursive method N!=" + metRec(N));
    }
}

/*Giving a vector of N elements, display the maximum element in the vector.*/

package g30125.maier.andreea.l2.e4;

import java.util.Scanner;

public class Exercitiul4 {
    public static void main(String[] args) {
        int Vector[];
        System.out.print("Enter the number of elements N= ");
        Scanner in = new Scanner (System.in);
        int N = in.nextInt();
        Vector = new int[N];

        System.out.println("Enter the elements: ");
        for (int i=0; i<N; i++) {
            Vector[i] = in.nextInt();
        }
        in.close();
        int maxElem = Vector[0];

        for (int i=1; i<N; i++) {
            if (Vector[i] > maxElem) {
                maxElem = Vector[i];
            }
        }
        System.out.println("The maximum element in the vector is: " + maxElem);
    }
}

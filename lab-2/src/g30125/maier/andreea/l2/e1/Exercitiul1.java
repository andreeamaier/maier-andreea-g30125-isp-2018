package g30125.maier.andreea.l2.e1;

import java.util.Scanner;

public class Exercitiul1 {
    public static void main(String[] args) {
        System.out.println("Enter two numbers: ");
        Scanner in = new Scanner (System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        in.close();

        if (x>y){
            System.out.println("Maximum number is: "+x);
        } else {
            System.out.println("Maximum number is: "+y);
        }
    }
}

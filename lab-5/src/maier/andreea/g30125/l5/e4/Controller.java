package maier.andreea.g30125.l5.e4;

public class Controller {

    private static volatile Controller sensor;

    private Controller() {}

    public static Controller getSensor() {
        synchronized (Controller.class) {
            if (sensor == null) {
                sensor = new Controller();
            }
        }
        return sensor;
    }
}
package maier.andreea.g30125.l5.e2;

import org.junit.Test;


public class TestImage {

    public static void main(String[] args) {

        ProxyImage img1 = new ProxyImage("TestImage1.png");
        ProxyImage img2 = new ProxyImage("TestImage2");

        img1.display();
        img2.display();

    }
}


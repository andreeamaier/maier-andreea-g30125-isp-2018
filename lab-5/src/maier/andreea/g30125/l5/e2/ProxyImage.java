package maier.andreea.g30125.l5.e2;

public class ProxyImage implements Image{

        private RealImage realImage;
        private RotatedImage rotatedImage;
        private String fileName;

        public ProxyImage(String fileName){
            this.fileName = fileName;
        }

        @Override
        public void display() {
            if (fileName.endsWith("png"))
            {
                realImage = new RealImage(fileName);
                realImage.display();
            }
            else {
                rotatedImage = new RotatedImage(fileName);
                rotatedImage.display();
            }
        }
}
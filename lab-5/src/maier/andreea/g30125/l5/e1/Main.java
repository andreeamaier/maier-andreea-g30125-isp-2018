package maier.andreea.g30125.l5.e1;

public class Main {

    public static void main(String[] args) {

        Circle c = new Circle("red",true,3);
        System.out.println(c.toString());
        System.out.println(c.getArea());

        Rectangle r = new Rectangle("blue",true,4,6);
        System.out.println(r.toString());
        System.out.println(r.getArea());

        Square s = new Square("green", false,10);
        System.out.println(s.toString());
        System.out.println(s.getPerimeter());
        System.out.println(s.getLength());
        System.out.println(c.getPerimeter());
    }
}

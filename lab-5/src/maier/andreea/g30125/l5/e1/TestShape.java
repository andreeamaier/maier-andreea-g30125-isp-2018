package maier.andreea.g30125.l5.e1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestShape {

    Shape[] shapes = new Shape[3];

    @Test
    public void ShouldGetArea() {
        shapes[0] = new Circle("red",true,3);
        shapes[1] = new Square("green", false,10);
        shapes[2] = new Rectangle("blue",true,4,6);
        assertEquals(28.259999999999998, shapes[0].getArea(),0.01);
        assertEquals(100, shapes[1].getArea(), 0.01);
        assertEquals(24,shapes[2].getArea(),0.01);
    }

    @Test
    public void ShoudGetPerimeter() {
        shapes[0] = new Circle("red",true,3);
        shapes[1] = new Square("green", false,10);
        shapes[2] = new Rectangle("blue",true,4,6);
        assertEquals(18.84, shapes[0].getPerimeter(),0.01);
        assertEquals(40, shapes[1].getPerimeter(), 0.01);
        assertEquals(20,shapes[2].getPerimeter(),0.01);
    }

}

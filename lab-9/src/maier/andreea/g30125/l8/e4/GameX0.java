package maier.andreea.g30125.l8.e4;

import maier.andreea.g30125.l8.e3.FileContent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameX0 extends JFrame {

    JButton t1,t2,t3,t4,t5,t6,t7,t8,t9;
    int k=0;

    GameX0(){
        setTitle("X & 0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,300);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        t1 = new JButton("");
        t1.setBounds(10,50,40,40);

        t2 = new JButton("");
        t2.setBounds(52,50,40,40);

        t3 = new JButton("");
        t3.setBounds(94,50,40,40);

        t4 = new JButton("");
        t4.setBounds(10,92,40,40);

        t5 = new JButton("");
        t5.setBounds(52,92,40,40);

        t6 = new JButton("");
        t6.setBounds(94,92,40,40);

        t7 = new JButton("");
        t7.setBounds(10,134,40,40);

        t8 = new JButton("");
        t8.setBounds(52,134,40,40);

        t9 = new JButton("");
        t9.setBounds(94,134,40,40);

        add(t1); add(t2); add(t3);
        add(t4); add(t5); add(t6);
        add(t7); add(t8); add(t9);
    }

    public static void main(String[] args) {
        new GameX0();
    }

    class checkIfPressed implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }

}

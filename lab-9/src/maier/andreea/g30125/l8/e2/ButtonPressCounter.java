package maier.andreea.g30125.l8.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonPressCounter extends JFrame {

    JButton button;
    JTextField textField;
    int k = 0;

    ButtonPressCounter(){

        setTitle("Button Press");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);

    }

    public void init(){
        this.setLayout(null);

        button = new JButton("Press ");
        button.setBackground(Color.pink);
        button.setBounds(10,50,100,30);

        textField = new JTextField();
        textField.setBounds(10,100,100,150);

        button.addActionListener(new checkIfPressed());

        add(textField);
        add(button);

    }

    public static void main(String[] args) {
        new ButtonPressCounter();
    }

    class checkIfPressed implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            k++;
            ButtonPressCounter.this.textField.setText(""+k);
        }
    }
}

/*A non-empty zero-indexed array A consisting of N integers is given. The array contains an odd number of elements,
and each element of the array can be paired with another element that has the same value, except for one element
that is left unpaired. Write a function that, given an array A consisting of N integers fulfilling the above conditions,
returns the value of the unpaired element.*/

package testalgo;

import java.util.Scanner;

public class Main {

    static void Solution(int A[]) {

        int index = 0;

        while (index < A.length) {
            int k = 0;

            for (int i = 0; i < A.length; i++) {
                if ((A[index] == A[i]) && (index != i)){
                    k++;
                }
            }

            if (k == 0)    //is true for the unpaired number
            {
                System.out.println("The number left unpaired is: " + A[index]);
            }

            index++;
        }
    }

    public static void main(String[] args) {

        System.out.println("Enter the number of elements: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int arr[] = new int[n];

        System.out.println("Enter array's elements:");
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        in.close();
        Solution(arr);
    }
}

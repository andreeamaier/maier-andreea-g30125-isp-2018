package com.company;

import becker.robots.*;

public class FetchingTheNewspaper {
    public static void main(String[] args) {
        City ny = new City();
        Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
        Thing newspaper = new Thing(ny, 2, 2);
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve1 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve3 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve4 = new Wall(ny, 1, 2, Direction.SOUTH);
        Wall blockAve5 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve6 = new Wall(ny, 2, 1, Direction.WEST);

        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.pickThing();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();

    }
}

package com.company;

import becker.robots.*;

public class MoveNorth {
    public static void main(String[] args) {

        City prague = new City();
        Robot karel = new Robot(prague, 1, 1, Direction.NORTH);

        karel.move();
        karel.move();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.move();
        karel.move();
    }
}

package com.company;

import becker.robots.*;

public class GoAroundTheWalls {
    public static void main(String[] args) {

        City ny = new City();
        Robot karel = new Robot(ny, 0, 2, Direction.WEST);
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve1 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve3 = new Wall(ny, 2, 2, Direction.EAST);
        Wall blockAve4 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve5 = new Wall(ny, 2, 2, Direction.SOUTH);
        Wall blockAve6 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve7 = new Wall(ny, 2, 1, Direction.WEST);

        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
    }
}

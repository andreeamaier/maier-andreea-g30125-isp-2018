package maier.andreea.g30125.l8.e1;

public class CoffeTest {
    public static void main(String[] args) throws Exception {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        try {
            for (int i = 0; i < 15; i++) {
                Cofee c = mk.makeCofee();
                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the cofee cup.\n");
                }
            }
        } catch (NumberException e){
            System.out.println("Exception: " + e.getMessage() + e.getN());
        } finally {
            System.out.println("Finally block.");
        }
    }
}



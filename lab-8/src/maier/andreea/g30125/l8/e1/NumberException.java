package maier.andreea.g30125.l8.e1;

public class NumberException extends Exception {
    int n;

    public NumberException(String msg,int n) {
        super(msg);
        this.n= n;
    }

    public int getN() {
        return n;
    }
}

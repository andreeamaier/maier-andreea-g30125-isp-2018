package maier.andreea.g30125.l8.e1;

public class CofeeMaker {
    private static int CofeeNumbers = 0;
    public final int maximumNoOfCofees = 2;
    Cofee makeCofee()throws NumberException{
        if (CofeeNumbers < maximumNoOfCofees) {
            System.out.println("Make a coffe");
            int t = (int) (Math.random() * 100);
            int c = (int) (Math.random() * 100);
            Cofee cofee = new Cofee(t, c);
            CofeeNumbers++;
            return cofee;
        } else throw new NumberException("The maximum number of coffees is ", maximumNoOfCofees);
    }


}

package maier.andreea.g30125.l8.e2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        FileReader newFile = new FileReader();
        System.out.println("Enter the character:");
        Scanner in = new Scanner(System.in);
        char c = in.next().charAt(0);
        in.close();
        System.out.println("The character appears " + newFile.countCharacter("data",c) + " times.");
    }
}

package maier.andreea.g30125.l8.e2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {

    String filename;


    public int countCharacter(String filename, char c){
        try {
            int count = 0;
            FileInputStream fis = new FileInputStream(filename);
            char current;
            while (fis.available() > 0){
                current = (char) fis.read();
                if (current == c)
                    count++;
            }
            return count;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("This file cannot be found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
